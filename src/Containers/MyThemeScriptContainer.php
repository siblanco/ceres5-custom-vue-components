<?php

namespace MyTheme\Containers;

use Plenty\Plugin\Templates\Twig;

class MyThemeScriptContainer
{
    public function call(Twig $twig):string
    {
        return $twig->render('MyTheme::Script');
    }
}