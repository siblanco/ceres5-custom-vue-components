<?php

namespace MyTheme\Containers;

use Plenty\Plugin\Templates\Twig;

class MyThemeStyleContainer
{
    public function call(Twig $twig):string
    {
        return $twig->render('MyTheme::Stylesheet');
    }
}