<?php

namespace MyTheme\Containers;

use Plenty\Plugin\Templates\Twig;

class MyThemeCategoryItem
{
    public function call(Twig $twig): string
    {
        return $twig->render('MyTheme::ItemList.Components.CategoryItem');
    }
}
