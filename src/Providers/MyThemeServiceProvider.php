<?php

namespace MyTheme\Providers;

use Plenty\Plugin\ServiceProvider;
use Plenty\Plugin\Events\Dispatcher;
use Plenty\Plugin\Templates\Twig;
use IO\Extensions\Functions\Partial;

class MyThemeServiceProvider extends ServiceProvider
{

    /**
     * Register the service provider.
     */
    public function register()
    {
    }

    /**
     * Boot a template for the footer that will be displayed in the template plugin instead of the original footer.
     */
    public function boot(Twig $twig, Dispatcher $dispatcher)
    {

        // override partials
        $dispatcher->listen('IO.init.templates', function (Partial $partial) {
            $partial->set('page-design', 'MyTheme::PageDesign.PageDesign');
            return false;
        }, 0);
    }
}
